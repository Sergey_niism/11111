#include <iostream>

using namespace std;


 //template < typename type> void mat_cout (type** first_element_pointer,int ArraySize);
template <typename type> type** connect_matrix (type* first_element_pointer1,int StringNumber, int ColumnNumber);
//double** connect_matrix (double* first_element_pointer1,int StringNumber, int ColumnNumber);


template <typename type> type** copy_matrix (type** first_element_pointer,int StringNumber, int ColumnNumber);

template <typename type> void delete_matrix (type** first_element_pointer,int StringNumber, int ColumnNumber);


template <typename type> void mat_cout (type** first_element_pointer,int StringNumber, int ColumnNumber, int flag_endl=1);
template <typename type> void mat_cout (type** first_element_pointer,int StringNumber, int ColumnNumber, string matrix_name);

template <typename type> void vector_cout (type* first_element_pointer1,int StringNumber,  string vector_name);


template <typename type> void fGaus_fist_step (type** first_element_pointer,int StringNumber, int ColumnNumber);
template <typename type,typename type2> void fGaus_second_step (type** first_element_pointer,
                                                                int StringNumber, int ColumnNumber, type2* decision);

template <typename type> type** revers_matrix (type** first_element_pointer,int StringNumber, int ColumnNumber);

template <typename type> type** cut_matrix (type** first_element_pointer,int StringNumber);

template <typename type> type** matrix_multiplication (type** f_point_matrix1,type** f_point_matrix2,int StringNumber);


int main()
{
    string command;
    cout << "Using standard input? Press \"y\" to agreement "<< endl;
    cin >> command;
    double** ap;

     //int n,m;

     int n=0,m=0;
//    if (command !="y")
//   {
        cout << "Input n "<< endl;
        cin >> n;
        m=n+1;
        cout << "Your n = "<< n <<", m = " << m << endl;
        double A[n][m];
        for (int i=0;i<n;i++)
        {
            int j;
            cout << "Input ";
            for (j=0; j<m-1;j++)
            {
                cout <<"a["<< i << "]["<<j<<"]"<<"   ";
            }
            cout << "b[" << i <<"]"<< endl;
            for (int j=0; j<m;j++)
            {
                cin >> A[i][j];
            }
        }
        cout << "Input success "<< endl;
        ap = connect_matrix (A[0],n,m);
        cout << "ap = "<<ap <<endl;
//    }
//    else
//    {
//         int n1=4;
//         int m1=n1+1;
//        double A[n1][m1];

//        A[0][0]= 10;
//        A[0][1]= 6;
//        A[0][2]= 2;
//        A[0][3]= 0;
//        A[0][4]= 25;

//        A[1][0]= 5;
//        A[1][1]= 1;
//        A[1][2]= -2;
//        A[1][3]= 4;
//        A[1][4]= 14;

//        A[2][0]= 3;
//        A[2][1]= 5;
//        A[2][2]= 1;
//        A[2][3]= -1;
//        A[2][4]= 10;

//        A[3][0]= 0;
//        A[3][1]= 6;
//        A[3][2]= -2;
//        A[3][3]= 2;
//        A[3][4]= 8;

//        n=n1;
//        m=m1;
//       ap = connect_matrix (A[0],n,m);
//       cout << "ap = "<<ap <<endl;
//   }

       cout<< fixed;

       cout << "ap = "<<ap <<endl;
       cout << "n = "<<n <<endl;
       cout << "m = "<<m <<endl;
//       cout<< endl;
//       cout << "(0,0) =  "<<ap[0][0]<<endl;
//       cout << "(0,0) =  "<<ap[0][0]<<endl;
//       cout << "(0,0) =  "<<ap[0][0]<<endl;
//       cout<< endl;

   mat_cout(ap,n,m,"A");

//   cout <<endl << "(0,0) =  "<<ap[0][0]<<endl;
//   cout << "(0,0) =  "<<ap[0][0]<<endl;
//   cout << "(0,0) =  "<<ap[0][0]<<endl;
//   cout << "(0,0) =  "<<ap[0][0]<<endl;
//   cout << "(0,0) =  "<<ap[0][0]<<endl;
//   cout << "(0,0) =  "<<ap[0][0]<<endl;
//   cout << "(0,0) =  "<<ap[0][0]<<endl<<endl;

   double ** bp = copy_matrix(ap,n,m);
   mat_cout(bp, n, m, "B");

   fGaus_fist_step(bp,n,m);
   mat_cout  <double> (bp,n,m);

   double * xp = new double [m-1];

   fGaus_second_step(bp,n,m,xp);
   vector_cout(xp,m-1,"decision");

   double ** reverse_A = revers_matrix(ap,n,m);
   mat_cout(reverse_A,n,n, "revers_A");

   double** square_A = cut_matrix(ap,n);
   mat_cout(square_A,n,n,"squara A");

   double ** multip = matrix_multiplication(square_A, reverse_A,n);
   mat_cout(multip,n,n,"A *A-1");

   multip = matrix_multiplication(reverse_A, square_A,n);
   mat_cout(multip,n,n,"A *A-1");



    cout << "Hello World!" << endl;
    return 0;
}



// РЕАЛИЗАЦИЯ ФУНКЦИЙ
//************************************************************************************************************
template <typename type> type** connect_matrix (type* first_element_pointer1,int StringNumber, int ColumnNumber)
{
    type (**pointer2)= new type* [StringNumber];
    cout<< endl<<endl<<"connect matrix start" <<endl;
    for (int i=0; i<StringNumber;i++)
    {
        pointer2 [i]= first_element_pointer1+i*ColumnNumber;
        cout <<"i = " << i<<"      "<<pointer2[i] << "     "<< first_element_pointer1+i*ColumnNumber<<endl;

        for (int j = 0; j<ColumnNumber;j++)
            cout << pointer2[i][j]<<"   ";
        cout << endl;
    }
     cout<<endl<<"pointer2 = "<< pointer2<< endl <<endl;
     cout<<"connect matrix end"<< endl <<endl;
    return pointer2;
}

//double ** connect_matrix (double* first_element_pointer1,int StringNumber, int ColumnNumber)
//{
//    double (**pointer2)= new double* [StringNumber];
//    cout<< endl<<endl<<"connect matrix start" <<endl;
//    for (int i=0; i<StringNumber;i++)
//    {
//        pointer2 [i]= first_element_pointer1+i*ColumnNumber;
//        cout <<"i = " << i<<"      "<<pointer2[i] << "     "<< first_element_pointer1+i*ColumnNumber<<endl;

//        for (int j = 0; j<ColumnNumber;j++)
//            cout << pointer2[i][j]<<"   ";
//        cout << endl;
//    }
//     cout<<endl<<"pointer2 = "<< pointer2<< endl <<endl;
//     cout<<"connect matrix end"<< endl <<endl;
//    return pointer2;
//}


template <typename type> type** copy_matrix (type** first_element_pointer,int StringNumber, int ColumnNumber)
{
    type** pointer2 = new type* [StringNumber];
    for (int i = 0; i<StringNumber; i++)
    {
        pointer2 [i]= new type [ColumnNumber];
        for (int j = 0; j<ColumnNumber;j++)
        {
            pointer2 [i] [j] = first_element_pointer [i][j];
        }
    }
    return pointer2;
}


template <typename type> void delete_matrix (type** first_element_pointer,int StringNumber, int ColumnNumber)
{
    for (int i = 0; i< StringNumber;i++)
    delete [] first_element_pointer[i];

    delete [] first_element_pointer;
}



 template <typename type> void mat_cout (type** first_element_pointer,int StringNumber, int ColumnNumber, int flag_endl=1)
{
    if (flag_endl) cout<<endl<<endl;
    if (StringNumber!= ColumnNumber)
    {for (int i=0;i<StringNumber;i++)
    {
        int j=0;
        for (;j<ColumnNumber-1;j++)
        {
           cout<<first_element_pointer [i][j]<<"\t" ;
        }
        cout<<"\t" <<first_element_pointer [i][j]<< "\t";

        cout<<endl;
    }
    }
    else
    {
        for (int i=0;i<StringNumber;i++)
            {
                int j=0;
                for (;j<StringNumber;j++)
                {
                    //type
                   // My_around()
                   cout<<first_element_pointer [i][j]<<"\t" ;
                }
                cout<<endl;
            }

    }
    cout<<endl<<endl;
}




template <typename type> void mat_cout (type** first_element_pointer,int StringNumber, int ColumnNumber, string matrix_name)
{
    cout << "Matrix " << matrix_name <<" = " << endl;
    mat_cout ( first_element_pointer, StringNumber, ColumnNumber, 0);
}

template <typename type> void vector_cout (type* first_element_pointer1,int StringNumber, string vector_name)
{
    cout<<endl << "Vector "<< vector_name << " = "<< endl;
    for (int i = 0; i<StringNumber; i++)
    {
        cout << "\t" << first_element_pointer1[i]<< endl;
    }
    cout<< endl;
}



template <typename type> void fGaus_fist_step (type** first_element_pointer,int StringNumber, int ColumnNumber)
{
    type mu;
    for (int P_start=0;P_start <ColumnNumber-2;P_start++)
    {
        //cout << "P_start = " << P_start<<endl;
        //mat_cout(first_element_pointer,StringNumber, ColumnNumber, 0);
       // int j=P_start;

        for (int i=P_start; i<StringNumber-1;i++)
        {

            mu = first_element_pointer[i+1][P_start]/first_element_pointer[P_start][P_start];
           // cout << "mu = " << mu<<endl;
            first_element_pointer[i+1][P_start]=0;

            for (int j = P_start+1; j< ColumnNumber;j++)
            {
                first_element_pointer[i+1][j] = first_element_pointer[i+1][j] - mu*first_element_pointer[P_start][j];
              //  cout<< first_element_pointer [i+1][j] <<"   ";
            }
            //cout << endl;
        }
        //cout << endl<<endl<<"Matrix = "<< endl;
        //mat_cout(first_element_pointer,StringNumber, ColumnNumber, 0);

    }
    cout <<endl<< "first step Gaus end"<<endl<<endl;
}


template <typename type, typename type2> void fGaus_second_step (type** first_element_pointer,
                                                                 int StringNumber, int ColumnNumber, type2* decision)
{
    type buff;
    for (int i = StringNumber-1; i>=0;i--)
    {
         buff = first_element_pointer[i][ColumnNumber-1];
         for (int j=ColumnNumber-1; j>i;j--)
             //int j=StringNumber-1;
             //while (j>i)
         {
             buff = buff - (first_element_pointer[i][j])* decision[j];
            // j--;
         }

         buff= buff/first_element_pointer[i][i];
         decision[i] = (type2) buff;
         //decision[i] =  buff;
    }
    cout <<"second step Gaus end"<<endl;
}


template <typename type> type** cut_matrix (type** first_element_pointer,int StringNumber)
{
    type** b = new type* [StringNumber];
    for (int i =0; i<StringNumber;i++)
    {
        b[i] = new type[StringNumber];
        for (int j=0; j<StringNumber;j++)
        {
            b[i][j] = first_element_pointer[i][j];
        }
    }
    //mat_cout(b,StringNumber, StringNumber,"cut matrix");
    return b;
}


template <typename type> type** revers_matrix (type** first_element_pointer,int StringNumber, int ColumnNumber)
{
    type** b;
    type** reverse = copy_matrix(first_element_pointer,StringNumber, ColumnNumber);
    type * B_vector = new type [StringNumber];
    cout <<"Start reverse matrix"<<endl;

    for (int i =0; i<StringNumber; i++)
    {
        b = copy_matrix(first_element_pointer,StringNumber, ColumnNumber);
        for (int j = 0; j<StringNumber; j++)
        {
            if (j==i){b[j][ColumnNumber-1]=1;}
            else {b[j][ColumnNumber-1]=0;}
        }

       // mat_cout(b,StringNumber, ColumnNumber);

        fGaus_fist_step(b,StringNumber, ColumnNumber);
        fGaus_second_step(b,StringNumber, ColumnNumber,B_vector);
       // vector_cout (B_vector, StringNumber, "");

        for (int j=0; j<StringNumber;j++)
        {
            reverse[j][i] = B_vector[j];
        }
       // mat_cout(reverse,StringNumber, ColumnNumber,"step end");
    }

    delete_matrix(b,StringNumber,ColumnNumber);
    delete [] B_vector;
    type** out =  cut_matrix(reverse, StringNumber);
    delete_matrix (reverse, StringNumber, ColumnNumber);
    return out;
}


template <typename type> type** matrix_multiplication (type** f_point_matrix1,type** f_point_matrix2,int StringNumber)
{
    type** result = new type* [StringNumber];
    for (int i =0; i<StringNumber; i++)
    {
        result[i]= new type [StringNumber];
        for (int j=0; j<StringNumber;j++)
        {
            type buff=0;
            for (int k =0; k< StringNumber;k++)
            {
                buff = buff + f_point_matrix1[i][k]*f_point_matrix2[k][j];
                result[i][j]=buff;
            }
        }
    }
   // mat_cout(result,StringNumber, StringNumber);
    return result;
}









